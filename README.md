[TOC]

**解决方案介绍**
===============
该解决方案能帮您快速在华为云上部署云容器引擎 CCE集群，并通过Nginx Ingress可以基于权重实现灰度发布。在初始灰度的时候就可以发现、调整问题，以保证其影响度，从而保证整体系统的稳定。

解决方案实践详情页面地址：https://www.huaweicloud.com/solution/implementations/gray-scale-publishing-based-on-nginx-ingress.html

**架构图**
---------------
![架构图](./document/gray-scale-publishing-based-on-nginx-ingress.png)
**架构描述**
---------------
该解决方案会部署如下资源：
1. 创建一台弹性云服务器ECS，纳管为CCE集群的Node节点，将Nginx-v1、Nginx-v2和Nginx Ingress三个工作负载调度至该节点上。
2. 部署2个弹性公网EIP并关联弹性负载均衡ELB和CCE集群，允许公网访问ELB和CCE集群的API接口。
3. 部署1个弹性负载均衡ELB，Nginx Ingress的service类型为Loadbalancer，需要绑定弹性负载均衡。
4. 部署一个CCE集群，并将弹性云服务器纳管为CCE集群的Node节点。

**组织结构**
---------------

``` lua
huaweicloud-solution-gray-scale-publishing-based-on-nginx-ingress-architecture
├── gray-scale-publishing-based-on-nginx-ingress.tf.json -- 资源编排模板
```
**开始使用**
---------------
1、重置密码

登录[华为云服务器控制台](https://console.huaweicloud.com/ecm/?agencyId=084d9251a8bf46ef9c4d7c408f8b21e8&region=cn-north-4&locale=zh-cn#/ecs/manager/vmList)，区域选择“北京四”，参考官网[重置弹性云服务器密码](https://support.huaweicloud.com/usermanual-ecs/zh-cn_topic_0067909751.html)，修改弹性云服务器初始化密码。

图1 华为云控制台

![华为云控制台](./document/readme-image-001.png)

图2 重置密码

![重置密码](./document/readme-image-002.png)

2、查看负载均衡实例

在[弹性负载均衡ELB控制台](https://console.huaweicloud.com/vpc/?agencyId=084d9251a8bf46ef9c4d7c408f8b21e8&region=cn-north-4&locale=zh-cn#/elb/list)，查看该方案一键部署创建的ELB实例及其绑定的弹性公网IP。

图3 ELB实例
![ELB实例](./document/readme-image-003.png)

3、查看CCE实例

在[云容器引擎CCE控制台](https://console.huaweicloud.com/cce2.0/?region=cn-north-4#/cce/cluster/list)，查看该方案一键部署生成的CCE实例。

图4 CCE实例
![CCE实例](./document/readme-image-004.png)

4、查看弹性云服务器实例

在[弹性云服务器控制台](https://console.huaweicloud.com/ecm/?agencyId=084d9251a8bf46ef9c4d7c408f8b21e8&region=cn-north-4&locale=zh-cn#/ecs/manager/vmList)，查看该方案一键部署创建的弹性云服务器ECS实例。

图5 ECS实例
![ECS实例](./document/readme-image-005.png)

5、登录到[华为SWR](https://console.huaweicloud.com/swr/?agencyId=990e2523bd104c61b5ac0cbea4ddbf3e&region=cn-north-4&locale=zh-cn#/swr/dashboard)页面，将容器镜像上传至华为SWR。镜像文件需带上tag信息，否则上传后版本为latest，具体请参考[如何创建镜像文件](https://support.huaweicloud.com/usermanual-swr/swr_01_0006.html)。

6、在[CCE控制台](https://console.huaweicloud.com/cce2.0/?region=cn-north-4#/cce/cluster/list)创建工作负载，选择“集群名称>工作负载”来创建负载。

图6 创建生产版本负载
![创建生产版本负载](./document/readme-image-006.png)
图7 创建灰度版本负载
![创建生产版本负载](./document/readme-image-007.png)

7、创建服务：选择“服务发现>创建服务”。

图8 创建生产ingress
![创建生产版本负载](./document/readme-image-008.png)
图9 创建灰度ingress
![创建生产版本负载](./document/readme-image-009.png)

8、在弹性云服务器控制台-选择node节点-远程登录，在node节点上通过负载均衡器的私网IP访问(由于节点并未绑定EIP所以无法通过EIP访问)，只有20%的流量转发到v2(灰度)版本。

图10 访问效果展示
![访问效果展示](./document/readme-image-010.png)